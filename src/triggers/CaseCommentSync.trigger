trigger CaseCommentSync on CaseComment (after insert, after update) {
   
    // Check whether current user is not JIRA agent so that we don't create an infinite loop.
    if (JIRA.currentUserIsNotJiraAgent()) {
        for (CaseComment cc : Trigger.new) {  
            String objectType ='CASE'; // Please change this according to the object type.      
            String objectId = cc.ParentId;
            // Calls the actual callout to synchronize with the JIRA issue.
            JIRAConnectorWebserviceCalloutSync.synchronizeWithJIRAIssue(JIRA.baseUrl, JIRA.systemId, objectType, objectId);
        }
    }

}